ARG OPENIAM_VERSION_NUMBER

FROM ubuntu:bionic-20210222
MAINTAINER "OpenIAM DEV Team(dev-team@openiam.com)"

ENV HELM_VERSION=3.3.4
ENV TERRAFORM_VERSION=0.12.21
ENV AZURE_CLI_VERSION=2.26.0

ENV OPENIAM_VERSION_NUMBER=4.2.1
ENV AZURE_REGION=WESTUS

LABEL name="openiamdocker/terraform" \
      vendor="OpenIAM" \
      version="$OPENIAM_VERSION_NUMBER" \
      release="0" \
      summary="OpenIAM Terraform Distribution" \
      description="This container contains Terraform and Helm for building out our terraform stuff" \
      url="https://www.openiam.com" \
      run='docker run openiamdocker/terraform:$OPENIAM_VERSION_NUMBER'

RUN apt update
RUN apt-get install -y git wget curl tar unzip sudo vim gettext-base apt-transport-https ca-certificates \
    apt-transport-https ca-certificates gnupg

RUN wget "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz"
RUN tar -zxvf helm-v${HELM_VERSION}-linux-amd64.tar.gz
RUN sudo mv linux-amd64/helm /usr/local/bin/helm

RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN sudo mv terraform /usr/local/bin/terraform

RUN curl -LO "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
RUN sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

COPY resources/*.sh /usr/local/bin/

# executing user has to be root b/c we need to do permissions operations
USER root
ENTRYPOINT ["docker-entrypoint.sh"]
